/** @format */

const formatOrderBook = data => {
  let orderBook = data.map(item => ({
    price: item["price"].toFixed(2),
    amount: item["quantity"],
    total: (item["price"] * item["quantity"]).toFixed(2)
  }));
  return orderBook;
};

const formatStateOrderBook = data => {
  let orderBook = data.map(item => ({
    price: item.price,
    amount: item.amount,
    total: item.total
  }));
  return orderBook;
};



const _innerCleanOrderBook = (newLevel, orderBook, side) => {
  let updatedOrderBook = orderBook.filter(item => {
    return item.price !== newLevel.price && item;
  });
  updatedOrderBook.push(newLevel);
  updatedOrderBook.sort((a, b) => {
    return (b.price - a.price) * side;
  });
  return updatedOrderBook;
};

const updateOrderBook = (that, data) => {
  let body = data.body;
  let newLevel = {
    new: true,
    price: body["price"].toFixed(2),
    amount: body["quantity"],
    total: (body["price"] * body["quantity"]).toFixed(2)
  };
  let orderBook =
    body.side === 1 ? that.state.bidOrderBook : that.state.askOrderBook;
  let updatedOrderBook = _innerCleanOrderBook(newLevel, orderBook, body.side);
  updatedOrderBook = updatedOrderBook.slice(0, that.state.defaultLevel);

  if (body.side === 1) {
    that.setState({
      bidOrderBook: updatedOrderBook,
      askOrderBook: formatStateOrderBook(that.state.askOrderBook)
    });
  } else {
    that.setState({
      bidOrderBook: formatStateOrderBook(that.state.bidOrderBook),
      askOrderBook: updatedOrderBook
    });
  }
};

const deleteOrderBookLevel = (that, data) => {
  let body = data.body;
  let deleteLevelPrice = body["price"].toFixed(2);
  let orderBook =
    body.side === 1 ? that.state.bidOrderBook : that.state.askOrderBook;
  let updatedOrderBook = orderBook.filter(item => {
    return item.price !== deleteLevelPrice && item;
  });
  updatedOrderBook = updatedOrderBook.slice(0, that.state.defaultLevel);

  that.setState(
    body.side === 1
      ? { bidOrderBook: updatedOrderBook }
      : { askOrderBook: updatedOrderBook }
  );
  return [];
};

export {
  updateOrderBook,
  deleteOrderBookLevel,
  formatOrderBook
};
