/** @format */

import React from "react";
import { Divider } from "antd";
import { Layout } from "antd";
import { Tabs } from "antd";
import { Row, Col } from "antd";
import Websocket from "react-websocket";
import OrderBook from "./components/OrderBook";
import TradeCard from "./components/TradeCard";
import { OpenOrder } from "./components/OpenOrders"
import { apiReqOrderBook, apiReqOpenOrders, apiReqCancelOrder } from "./rest"

import {
  updateOrderBook,
  deleteOrderBookLevel,
} from "./utils";

import "./App.css";

const { TabPane } = Tabs;
const { Header, Footer, Content } = Layout;

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      user_id: 1, 
      defaultLevel: 5,
      instrument: "BTC/USD",
      lading: false,
      openOrders: [],
      askOrderBook: [],
      bidOrderBook: []
    };
  }


  componentDidMount() {
    let that = this
    apiReqOrderBook(that)
    apiReqOpenOrders(that)
  }

  cancelOrderHandler(record) {
    let that = this
    apiReqCancelOrder(that, record.id)
  }

  updateOpenOrder() {
    let that = this
    apiReqOpenOrders(that);
  } 

  handleWebSocketMessage(rawData) {
    let data = JSON.parse(rawData);
    if (!data.msg_item) {
      return;
    }

    const actions = {
      order_book__update: data => {
        // update order book
        let that = this;
        updateOrderBook(that, data);
      },
      order_book__delete: data => {
        // delete order book level
        let that = this;
        deleteOrderBookLevel(that, data);
      },
      default: () => {
        return;
      }
    };

    let action =
      actions[`${data.msg_item}__${data.action}`] || actions["default"];
    action.call(this, data);
  }

  render() {
    return (
      <div className="App">
        <Header>
          <h1 className="instrument" style={{ color: "white" }}>
            Order Book : {this.state.instrument}
          </h1>
          <Websocket
            url="ws://18.163.169.210:9900"
            onMessage={r => this.handleWebSocketMessage(r)}
          />
        </Header>
        <Content>
          <TradeCard 
            updateOpenOrder={ () => this.updateOpenOrder() }/>
          <Row>
            <Col span={14} offset={5}>
              <Tabs type="card">
                <TabPane tab="Order Book" key="1">
                  <OrderBook
                    askOrderBook={this.state.askOrderBook}
                    bidOrderBook={this.state.bidOrderBook}
                  />
                </TabPane>
                <TabPane tab="Open Orders" key="2">
                  <div span={6} offset={6}>
                    <OpenOrder 
                      userOpenOrderList={this.state.openOrders}
                      cancelOrderHandler={(record) => this.cancelOrderHandler(record)}
                    />
                  </div>
                </TabPane>
              </Tabs>
            </Col>
          </Row>
        </Content>
        <Divider dashed />
        <Footer style={{ textAlign: "center" }}>©2020 Created by YQC</Footer>
      </div>
    );
  }
}

export default App;
