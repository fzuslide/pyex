/** @format */

import React, { Component } from "react";
import PropTypes from "prop-types";

import { Row, Col, Table } from "antd";

class OrderBook extends Component {
  render() {
    const buyColumns = [
      {
        title: "Total",
        dataIndex: "total"
      },
      {
        title: "Amount",
        dataIndex: "amount"
      },
      {
        title: "Price",
        dataIndex: "price"
      }
    ];
    const sellColumns = [
      {
        title: "Price",
        dataIndex: "price"
      },
      {
        title: "Amount",
        dataIndex: "amount"
      },
      {
        title: "Total",
        dataIndex: "total"
      }
    ];

    return (
      <div className="OrderBook">
        <Row>
          <Col span={8} offset={4}>
            <h4 >Buy Order:</h4>
            <Table
              columns={buyColumns}
              dataSource={this.props.bidOrderBook}
              pagination={false}
              rowClassName={(record) => record.new && 'new-buy-row'}
              size="small"
              className="buy-table"
              rowKey="price"
            />
          </Col>
          <Col span={8} offset={1}>
            <h4 >Sell Order:</h4>
            <Table
              columns={sellColumns}
              dataSource={this.props.askOrderBook}
              pagination={false}
              rowClassName={(record) => record.new && 'new-sell-row'}
              size="small"
              className="sell-table"
              rowKey="price"
            />
          </Col>
        </Row>
      </div>
    );
  }
}

OrderBook.propTypes = {
  askOrderBook: PropTypes.array,
  bidOrderBook: PropTypes.array
};

export default OrderBook;
