/** @format */

import React, { Component } from "react";
import { Form, InputNumber, Button } from "antd";

export default class OrderForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultPrice: 8000.5,
      defaultAmount: 1
    };
  }

  componentWillMount() {
    this.props.onInputChange(this.state.defaultPrice, 'price')
    this.props.onInputChange(this.state.defaultAmount, 'amount')
  }
  
  render() {
    const formAction = this.props.formAction
    console.log('render form', formAction)
    const formItemLayout = {
      labelCol: { span: 4, offset: 7 },
      wrapperCol: { span: 2 }
    };
    const buttonItemLayout = {
      wrapperCol: { span: 4, offset: 10 }
    }

    return (
      <div>
        <Form layout="horizontal">
          <Form.Item label="Price" {...formItemLayout}>
            <InputNumber
              defaultValue={this.state.defaultPrice}
              formatter={value =>
                `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={value => value.replace(/\$\s?|(,*)/g, "")}
              step={0.2}
              min={0.01}
              max={10000}
              placeholder="input price"
              size="large"
              required={true}
              style={{ width: 150 }}
              onChange={(e) => this.props.onInputChange(e, 'price')}
            />
          </Form.Item>
          <Form.Item label="Amount" {...formItemLayout}>
            <InputNumber
              placeholder="input amount"
              defaultValue={this.state.defaultAmount}
              min={1}
              max={1000}
              size="large"
              required={true}
              style={{ width: 150 }}
              onChange={(e) => this.props.onInputChange(e, 'amount')}
            />
          </Form.Item>
          <Form.Item {...buttonItemLayout}>
            <Button
              type="primary"
              onClick={() => this.props.handlePlaceOrder()}
              style={ formAction === 'Sell' ? { backgroundColor: "#E6005C", borderColor: "#E6005C", background: "#E6005C" }: {}}
              block
            >
              {formAction}
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}
