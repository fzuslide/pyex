/** @format */

import React, { Component } from "react";
import { Table, Button } from "antd";

class OpenOrder extends Component {
  constructor(props) {
    super(props);
    this.columns = this.props.getColumns.call(this);
    this.state = {};
  }

  static defaultProps = {
    getColumns: function() {
      return [
        {
          title: "Time",
          dataIndex: "time",
          key: "time"
        },
        {
          title: "Price",
          dataIndex: "price",
          key: "price"
        },
        {
          title: "Amount",
          dataIndex: "amount",
          key: "amount"
        },
        {
          title: "Leave Qty",
          dataIndex: "leaveQty",
          key: "leaveQty"
        },
        {
          title: "Action",
          key: "action",
          render: (text, record) => (
            <span>
              <Button
                type="dashed"
                onClick={() => (this.props.cancelOrderHandler(record))}
              >
                Cancel
              </Button>
            </span>
          )
        }
      ];
    }
  };

  render() {
    return (
      <div span={6} offset={6}>
        <Table
          columns={this.columns}
          dataSource={this.props.userOpenOrderList}
          rowKey={record => record.time}
          pagination={false}
          size="middle"
        />
      </div>
    );
  }
}

const formatTimeStamp = t => {
  var date = new Date(t * 1000);
  return (
    date.toLocaleDateString().replace(/\//g, "-") +
    " " +
    date.toTimeString().substr(0, 8)
  );
};

const formatOpenOrders = data => {
  let openOrders = data.map(item => ({
    id: item.id,
    time: formatTimeStamp(item.t),
    amount: item.quantity,
    leaveQty: item.leave_qty,
    price: item.price
  }));
  return openOrders;
};

export { OpenOrder, formatOpenOrders, formatTimeStamp };
