/** @format */

import React, { Component } from "react";
import { Card, Divider } from "antd";
import OrderForm from "./OrderForm";
import { apiReqCreateOrder } from "../rest"

export default class TradeCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: 1,
      actionKey: "Buy",
      price: 0,
      amount: 0
    };
  }

  onInputChange(value, field) {
    this.setState({
      [field]: value
    });
  }

  updateOpenOrder() {
    this.props.updateOpenOrder()
  } 

  handlePlaceOrder() {
    let that = this
    apiReqCreateOrder(that)
  }

  onTabChange = (key, type) => {
    this.setState({ actionKey: key });
  };

  switchRenderTradeCard() {
    return (
      <OrderForm
        formAction={this.state.actionKey}
        onInputChange={(value, field) => this.onInputChange(value, field)}
        handlePlaceOrder={() => this.handlePlaceOrder()}
      ></OrderForm>
    );
  }

  render() {
    const tabList = [
      {
        key: "Buy",
        tab: "Buy"
      },
      {
        key: "Sell",
        tab: "Sell"
      }
    ];

    return (
      <div>
        <Card
          style={{ width: "100%" }}
          tabList={tabList}
          activeTabKey={this.state.key}
          onTabChange={key => {
            this.onTabChange(key, "key");
          }}
        >
          {this.switchRenderTradeCard()}
        </Card>
        <Divider />
      </div>
    );
  }
}
