/** @format */

import axios from "axios";

import { formatOrderBook } from "./utils";
import { formatOpenOrders } from "./components/OpenOrders";

const apiReqOrderBook = that => {
  const orderBookUrl = `/api/order/query/order/book/`;
  axios
    .get(orderBookUrl, {
      params: {
        level: that.state.defaultLevel
      }
    })
    .then(response => {
      const result = response.data;
      if (result.ok) {
        const bidOrderBook = formatOrderBook(result.data.buy);
        const askOrderBook = formatOrderBook(result.data.sell);

        that.setState({
          loading: false,
          bidOrderBook: bidOrderBook,
          askOrderBook: askOrderBook
        });
      }
    })
    .catch(error => {
      console.log("order book fail", error);
    });
};

const apiReqOpenOrders = that => {
  const openOrderUrl = `/api/order/list/user/open/order/`;
  axios
    .get(openOrderUrl, {
      params: {
        user_id: that.state.user_id
      }
    })
    .then(response => {
      const result = response.data;
      if (result.ok) {
        that.setState({
          openOrders: formatOpenOrders(result.data)
        });
      }
    })
    .catch(error => {
      console.log("open order fail", error);
    });
};

const apiReqCancelOrder = (that, id) => {
  const url = `/api/order/cancel/`;
  axios
    .get(url, {
      params: {
        id: id
      }
    })
    .then(response => {
      const result = response.data;
      if (result.ok) {
        apiReqOpenOrders(that);
      }
    })
    .catch(error => {
      console.log("cancel order fail", error);
    });
};

const apiReqCreateOrder = (that) => {
  const url = `/api/order/create/`;
  let data = new FormData();
  data.append("user_id", that.state.user_id);
  data.append("side", that.state.actionKey === "Buy" ? 1 : -1);
  data.append("price", that.state.price);
  data.append("quantity", that.state.amount);
  data.append("order_type", "1"); // limit order

  axios
    .post(url, data)
    .then(response => {
      const result = response.data;
      if (result.ok) {
          that.updateOpenOrder()
      }
    })
    .catch(error => {
      console.log("create order fail", error);
    });
};

export {
  apiReqOrderBook,
  apiReqOpenOrders,
  apiReqCreateOrder,
  apiReqCancelOrder
};
