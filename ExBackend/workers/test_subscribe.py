#coding=utf-8

import os
import sys 

abs_path = os.path.abspath(__file__)
project_path = '/'.join(abs_path.split('/')[:-2])
sys.path.insert(0, project_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ExBackend.settings")

import django
django.setup()

import time
import simplejson
import datetime

from django.conf import settings

from utils.messaging import RedisConsumer

MESSAGER_HOST = str(settings.DEFAULT_REDIS_MESSAGE_HOST)
MESSAGER_PORT= settings.DEFAULT_REDIS_MESSAGE_PORT
MESSAGER_CHANNEL = str(settings.DEFAULT_REDIS_MESSAGE_CHANNEL)


if __name__ == '__main__':
    rc = RedisConsumer(MESSAGER_HOST, MESSAGER_PORT, MESSAGER_CHANNEL)
    rc.wait(cb = lambda x: print(x))
