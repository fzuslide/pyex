#coding=utf-8

import os
import sys 

abs_path = os.path.abspath(__file__)
project_path = '/'.join(abs_path.split('/')[:-3])
sys.path.insert(0, project_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ExBackend.settings")

import django
django.setup()

import time
import simplejson
import datetime

from django.conf import settings

from utils.messaging import RedisPublishClient

MESSAGER_HOST = str(settings.DEFAULT_REDIS_MESSAGE_HOST)
MESSAGER_PORT= settings.DEFAULT_REDIS_MESSAGE_PORT
MESSAGER_CHANNEL = str(settings.DEFAULT_REDIS_MESSAGE_CHANNEL)

class BaseBroadcastHandler(object):

    @classmethod
    def broadcast_msg(cls, msg, host=MESSAGER_HOST, port=MESSAGER_PORT, channel=MESSAGER_CHANNEL):
        publisher = RedisPublishClient(host, port, channel)
        r = publisher.publish_message(msg)



if __name__ == '__main__':
    msg = {
        "msg_type": "order", 
        "action": "create",
        "msg_id": 1,
        "order":{
            'user': 1,
            'side': 1,
            'price': '6021.50',
            'quantity': '3.5', 
            'leave_qty': '3.5',
            'status': 0,
            'order_type': 1, 
            'id': 6,
            't': 1582887664
            }
        }
    bbh = BaseBroadcastHandler()
    bbh.broadcast_msg(msg )
