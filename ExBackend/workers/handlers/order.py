#coding=utf-8

import simplejson
import time
import datetime
import os
import sys 

abs_path = os.path.abspath(__file__)
project_path = '/'.join(abs_path.split('/')[:-3])
sys.path.insert(0, project_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ExBackend.settings")

import django
django.setup()

from django.conf import settings

from engine.engine import Engine
from utils.timeutils import stamp_to_datetime

from workers.handlers.base_handler import BaseBroadcastHandler


class CreateOrderProcessHandler(BaseBroadcastHandler):
    """
    msg struct 

    msg = {
        "msg_type": "order", 
        "action": "create",
        "order":{
            'user': 1,
            'side': '1',
            'price': '8011.50',
            'quantity': '3.5', 
            'leave_qty': '3.5',
            'status': 0,
            'order_type': 1, 
            'id': 6,
            't': 1582887664
            }
        }
    """

    def __init__(self, msg):
        self.msg = msg

    def process(self):
        engine = Engine(self.msg['order'])
        engine.process_order()
        for raw_msg in  engine.publish_msg_list:
            self.broadcast_msg(raw_msg)
        return True

class CancelOrderProcessHandler(BaseBroadcastHandler):
    """
    Cancel Order
    """

    def __init__(self, msg):
        self.msg = msg

    def process(self):
        engine = Engine(self.msg['order'])
        engine.process_cancel()
        for raw_msg in  engine.publish_msg_list:
            self.broadcast_msg(raw_msg)
        return True


if __name__ == '__main__':
    msg = {
        "msg_type": "order", 
        "action": "create",
        "msg_id": 1,
        "order":{
            'user': 1,
            'side': -1,
            'price': '8982.50',
            'quantity': '1', 
            'leave_qty': '1',
            'status': 0,
            'order_type': 1, 
            'id': 236,
            't': 1582887664
            }
        }
    coph = CreateOrderProcessHandler(msg)
    coph.process()



