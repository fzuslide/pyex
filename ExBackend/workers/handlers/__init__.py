#coding=utf-8

from . import order

msg_handlers = {
        # order
        'order_create': order.CreateOrderProcessHandler,
        'order_cancel': order.CancelOrderProcessHandler,
        }

def get_msg_handler(msg):
    return msg_handlers.get('%s_%s' % (msg['msg_type'], msg['action']))


