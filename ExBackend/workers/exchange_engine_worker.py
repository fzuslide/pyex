#coding=utf-8

#import gevent
#from gevent import monkey; monkey.patch_all()

import os
import sys

abs_path = os.path.abspath(__file__)
project_path = '/'.join(abs_path.split('/')[:-2])
sys.path.insert(0, project_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ExBackend.settings")

import django
django.setup()

from django.conf import settings

import time
import zmq
import logging
import datetime
import traceback
import logging.handlers
import simplejson

from handlers import get_msg_handler

from utils.disklog import write_disk_log


# logger
ch = logging.handlers.TimedRotatingFileHandler(os.path.join(settings.ZMQ_LOG_PATH, settings.ZMQ_WORKER_LOG_FILE), 'midnight', 1)
ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
ch.suffix = '%Y%m%d'
for name in ['']:
    logger = logging.getLogger(name)
    logger.setLevel(settings.ZMQ_LOG_LEVEL)
    logger.addHandler(ch)

if len(sys.argv)>=2:
    ZMQ_HOST = sys.argv[1]
    ZMQ_PORT = sys.argv[2]
else:
    ZMQ_HOST = str(settings.DEFAULT_ZMQ_HOST)
    ZMQ_PORT = str(settings.DEFAULT_ZMQ_PORT)

context = zmq.Context()

receiver = context.socket(zmq.PULL)
receiver.connect('tcp://%s:%s'%(ZMQ_HOST, ZMQ_PORT))

counter = 0


def in_handler(msg):
    global counter
    Handler = get_msg_handler(msg)
    if Handler:
        handler = Handler(msg)
        logging.debug('processing: %d, %s' % (counter, msg_str))
        try:
            receive_msec = int(time.time()*1000)
            ok = handler.process()
            if ok:
                finish_msec = int(time.time()*1000)
                if settings.ZMQ_NEED_DISKLOG:
                    write_disk_log({
                        'doc_type': 'MessageProcessFinish',
                        'msg_id': counter,
                        'msg_type': msg['msg_type'],
                        'msg': msg,
                        'receive_msec': receive_msec,
                        'finish_msec': finish_msec,
                        'duration': finish_msec - receive_msec
                        })
        except Exception as e:
            traceback.print_exc()
            logging.error('processing message error: %s, msg: %s' % (str(e), msg_str))
        counter += 1
    else:
        logging.debug('no handler: %s' % (msg_str))



if __name__ == '__main__':
    while True:
        msg_str = receiver.recv()
        try:
            msg = simplejson.loads(msg_str)
        except Exception as e:
            traceback.print_exc()
            logging.error('receive message not valid, msg: %s' % msg_str)
            continue
        
        if not msg:
            continue
        in_handler(msg)

