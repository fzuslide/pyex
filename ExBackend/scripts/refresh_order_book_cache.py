#coding=utf-8

import os
import sys
import datetime

abs_path = os.path.abspath(__file__)
project_path = '/'.join(abs_path.split('/')[:-2])
sys.path.insert(0, project_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ExBackend.settings")

import django
django.setup()

from django.db.models import Q

from order.order_book import OrderBookCache
from order.models import UserOrder

def refresh_order_book_cache():
    OrderBookCache.clean_cache()
    order_list = UserOrder.objects.filter(status__in=[UserOrder.ORDER_STATUS[0][0], UserOrder.ORDER_STATUS[1][0]]).all()
    [OrderBookCache.add_order_to_side_order_book(x.get_json()) for x in order_list]


if __name__ == '__main__':
    refresh_order_book_cache()
