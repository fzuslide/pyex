#coding=utf-8
"""ExBackend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include

import xadmin
xadmin.autodiscover()

from xadmin.plugins import xversion
xversion.register_models()

class GlobalSetting(object):
    #设置base_site.html的Title
    site_title = 'Ex管理后台'
    #设置base_site.html的Footer
    site_footer  = 'Ex'
xadmin.site.register(xadmin.views.CommAdminView, GlobalSetting)

urlpatterns = [
    url(r'xadmin/', xadmin.site.urls),
    url(r'^api/user/', include('user_profile.urls')),
    url(r'^api/order/', include('order.urls')),
]
