# coding:utf-8

from django.apps import AppConfig
import os

default_app_config = 'order.ClientConfig'

VERBOSE_APP_NAME = "Order管理"


class ClientConfig(AppConfig):
    name = "order"
    verbose_name = VERBOSE_APP_NAME
