#coding=utf-8

import json
from enum import Enum

from django.db import models
from django.core import serializers
from django.conf import settings
from django.db.models import Q
from django.contrib.auth.models import User


from utils.basemodel.base import BaseModel
from utils.timeutils import datetime_to_stamp

epsilon = 0.000001

class UserOrder(BaseModel):
    """
    User order
    """
    SIDE_CHOICES = [
            (1, 'BUY'),
            (-1, 'SELL'),
            ]

    ORDER_STATUS = [
            (0, 'OPEN'),
            (1, 'PARTIAL_FILL'),
            (2, 'FILLED'),
            (3, 'CANCELLED'),
            ]

    ORDER_TYPES = [
            (1, 'LIMIT'),
            ]

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    side = models.IntegerField(choices=SIDE_CHOICES, db_index=True)
    price = models.DecimalField(max_digits=15, decimal_places=2) 
    quantity = models.DecimalField(max_digits=15, decimal_places=2) 
    leave_qty = models.DecimalField(max_digits=15, decimal_places=2, default=0) 
    status = models.IntegerField(choices=ORDER_STATUS, default=ORDER_STATUS[0][0], db_index=True)
    order_type = models.IntegerField('下单类别', choices=ORDER_TYPES, default=ORDER_TYPES[0][0])

    class Meta:
        db_table = 'user_order'
        verbose_name = 'Order'
        verbose_name_plural = 'Order'

    def __str__(self):
        return '%s:%s:%s' % (self.id, self.side, self.price)

    def get_json(self, clean=True):
        serials = serializers.serialize("json", [self])
        struct = json.loads(serials)
        data = struct[0]['fields']
        if 'pk' in struct[0]:
            data['id'] = struct[0]['pk']
        data['t'] = datetime_to_stamp(self.create_time)
        if clean:
            data.pop('update_time')
            data.pop('create_time')
            data.pop('is_active')
        return data 

    @classmethod
    def update_order_status(cls, id, leave_qty):
        obj = cls.objects.get(pk=id)
        if float(leave_qty) < epsilon:
            obj.leave_qty = 0
            obj.status = cls.ORDER_STATUS[2][0]
        else:
            obj.leave_qty = float(leave_qty)
            obj.status = cls.ORDER_STATUS[1][0]
        obj.save()
        return obj




