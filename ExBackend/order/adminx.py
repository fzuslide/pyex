#coding=utf-8

import xadmin
from django.contrib import admin
from django.utils.safestring import mark_safe
from import_export.admin import ExportMixin

from .models import UserOrder

class UserOrderAdmin(object):
    list_filter = ("status", "order_type", "side")
    list_display = ("id", "side", "price", "quantity", "leave_qty", "status", "order_type", "create_time", "update_time")

xadmin.site.register(UserOrder, UserOrderAdmin)
