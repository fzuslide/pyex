#coding=utf-8
"""
user Profile
"""

import logging
import datetime
import operator
from functools import reduce
from django.db import models
from django.db.models import Q
from django.conf import settings

import utils.errors as err
from utils.view_tools import ok_json, fail_json
from utils.abstract_api import AbstractAPI
from utils.zmq_msg_utils import zmq_dispatch_msg

from user_profile.models import UserProfile

from .models import UserOrder
from .order_book import OrderBookCache

logger = logging.getLogger(__name__)

class CreateUserOrderAPI(AbstractAPI):
    """
    Create User Order
    """

    def config_args(self):
        self.args = {
                'user_id': 'r',
                'side': 'r',
                'price': 'r',
                'quantity': 'r',
                'order_type': 'r'
                }

    def access_db(self, kwarg):
        try:
            if float(kwarg['price']) <= 0 and int(kwarg[quantity]) <= 0:
                return False, err.ERROR_CODE_ORDER_INVALID_ARGS
            user_order = UserOrder()
            user_order.user_id = kwarg['user_id']
            user_order.side = kwarg['side']
            user_order.price = kwarg['price']
            user_order.quantity = kwarg['quantity']
            user_order.leave_qty = kwarg['quantity']
            user_order.order_type = kwarg['order_type']
            user_order.save()
            # dispatch msg
            zmq_dispatch_msg({
                        'msg_type': 'order', 
                        'action': 'create',
                        'order': user_order.get_json() 
                        })

            return True, user_order
        except Exception as e:
            import traceback; traceback.print_exc()
            logger.error('[class:%s] %s' % (self.__class__.__name__, e))
            return False, err.ERROR_CODE_DATABASE_QUERY_ERROR

    def format_data(self, data):
        if data[0]:
            return ok_json(data=data[1].get_json())
        return fail_json(err.ERROR_CODE_DATABASE_QUERY_ERROR)

create_user_order_api = CreateUserOrderAPI().wrap_func()

class CancelUserOrderAPI(AbstractAPI):
    """
    Cancel  User Order
    """

    def config_args(self):
        self.args = {
                'id': 'r',
                }

    def access_db(self, kwarg):
        try:
            user_order = UserOrder.objects.get(pk=kwarg['id'])
            if user_order.status not in [UserOrder.ORDER_STATUS[0][0], UserOrder.ORDER_STATUS[1][0]]:
                return False, err.ERROR_CODE_ORDER_CANNOT_CANCEL_ORDER
            user_order.status = UserOrder.ORDER_STATUS[3][0]
            user_order.save()
            # dispatch msg
            zmq_dispatch_msg({
                        'msg_type': 'order', 
                        'action': 'cancel',
                        'order': user_order.get_json() 
                        })

            return True, user_order
        except Exception as e:
            import traceback; traceback.print_exc()
            logger.error('[class:%s] %s' % (self.__class__.__name__, e))
            return False, err.ERROR_CODE_DATABASE_QUERY_ERROR

    def format_data(self, data):
        if data[0]:
            return ok_json(data=data[1].get_json())
        return fail_json(err.ERROR_CODE_DATABASE_QUERY_ERROR)

cancel_user_order_api = CancelUserOrderAPI().wrap_func()

class QueryOrderBookAPI(AbstractAPI):
    def config_args(self):
        self.args = {
                'level': ('o', 5),
                }

    def access_db(self, kwarg):
        try:
            level = int(kwarg['level'])
            buy_order_book = OrderBookCache.get_side_order_book(UserOrder.SIDE_CHOICES[0][0], level)
            sell_order_book = OrderBookCache.get_side_order_book(UserOrder.SIDE_CHOICES[1][0], level)
            return True, {'buy': buy_order_book, 'sell': sell_order_book}
        except Exception as e:
            import traceback; traceback.print_exc()
            logger.error('[class:%s] %s' % (self.__class__.__name__, e))
            return False, None

    def format_data(self, data):
        ok, info = data
        if ok:
            return ok_json(data=info)
        return fail_json(err.ERROR_CODE_DATABASE_QUERY_ERROR)

query_order_book_api = QueryOrderBookAPI().wrap_func()


class ListUserOpenOrderAPI(AbstractAPI):
    def config_args(self):
        self.args = {
                'user_id': 'r',
                'limit': ('o', 10),
                }

    def access_db(self, kwarg):
        try:
            order_list = UserOrder.objects.filter(user_id=kwarg['user_id'], \
                    status__in=[UserOrder.ORDER_STATUS[0][0], UserOrder.ORDER_STATUS[1][0]]).order_by("-create_time").all()[:int(kwarg['limit'])]
            return True, order_list
        except Exception as e:
            import traceback; traceback.print_exc()
            logger.error('[class:%s] %s' % (self.__class__.__name__, e))
            return False, None

    def format_data(self, data):
        ok, order_list = data
        if ok:
            return ok_json(data=[x.get_json() for x in order_list])
        return fail_json(err.ERROR_CODE_DATABASE_QUERY_ERROR)

list_user_open_order_api = ListUserOpenOrderAPI().wrap_func()
