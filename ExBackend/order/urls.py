#coding=utf-8


from django.conf.urls import url
from .views import create_user_order_api, cancel_user_order_api, \
        query_order_book_api, list_user_open_order_api

urlpatterns = [
        # create
        url(r'^create/$', create_user_order_api, name="create_user_order_api"),
        # cancel
        url(r'^cancel/$', cancel_user_order_api, name="cancel_user_order_api"),

        # query
        url(r'^query/order/book/$', query_order_book_api, name="query_order_book_api"),
        url(r'^list/user/open/order/$', list_user_open_order_api, name="list_user_open_order_api"),
        ]

