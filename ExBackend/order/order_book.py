#coding=utf-8

import time
import random
import string
import logging
import warnings
from datetime import datetime
from typing import Optional

import simplejson as json
from redis import ConnectionPool, Redis

from django.conf import settings

pool = ConnectionPool(
    host=settings.ORDER_BOOK_REDIS_SERVER,
    port=settings.ORDER_BOOK_REDIS_PORT,
    password=settings.ORDER_BOOK_REDIS_PASSWORD,
    db=settings.ORDER_BOOK_REDIS_DB)

order_book_client = Redis(connection_pool=pool)




class OrderBookCache(object):
    """
    Order Book Cache

    order list cache:
        [[id, price, quantity, leave_qty, side, timestamp], ...]
    """

    PREFIX = 'OBC'
    ORDER_LIST_PRIFIX = 'OBC:OL:%s:%s'
    price_level_base_key = '%s:%s'
    rclient = order_book_client

    @classmethod
    def _make_order_book_key(cls, side):
        return '%s:LV:%s' % (cls.PREFIX, int(side))

    @classmethod
    def _make_level_list_key(cls, side, price):
        return cls.ORDER_LIST_PRIFIX % (int(side), float(price))

    @classmethod
    def cal_order_total_leave_qty(cls, order_list):
        return sum([float(x[3]) for x in order_list])

    @classmethod
    def get_order_book_level_key_list(cls, side, level):
        key = cls._make_order_book_key(side)
        if int(side) > 0:
            r = cls.rclient.zrange(key, 0, end=level-1, desc=True, withscores=True)
        else:
            r = cls.rclient.zrange(key, 0, end=level-1, desc=False, withscores=True)
        if r:
            return r
        return []

    @classmethod
    def set_order_book_level_key(cls, side, price):
        key = cls._make_order_book_key(side)
        r = cls.rclient.zadd(key, float(price), float(price))
        return r

    @classmethod
    def delete_order_book_level_key(cls, side, price):
        key = cls._make_order_book_key(side)
        r = cls.rclient.zrem(key, float(price), float(price))
        return r

    @classmethod
    def get_can_math_level_key_list(cls, side, price):
        key = cls._make_order_book_key(side)
        if int(side) > 0:
            r = cls.rclient.zrevrangebyscore(key, '+inf', float(price), withscores=True) 
        else:
            r = cls.rclient.zrangebyscore(key, 0, float(price), withscores=True)
        if r:
            return r
        return []

    @classmethod
    def get_level_order_list(cls, side, level_price):
        key = cls._make_level_list_key(side, level_price)
        order_list = cls.rclient.lrange(key, 0, -1)
        if order_list:
            return [v.decode('utf-8').split(',') for v in order_list]
        return []


    @classmethod
    def get_side_order_book(cls, side, level):
        key_list = cls.get_order_book_level_key_list(side, level) 
        side_order_book = []
        for item in key_list:
            price = item[0].decode('utf-8')
            lv_list = cls.get_level_order_list(side, price)
            if lv_list:
                leave_qty = cls.cal_order_total_leave_qty(lv_list)
                side_order_book.append({'quantity': leave_qty, 'price': float(price)})
        return side_order_book

    @classmethod
    def add_order_to_side_order_book(cls, info):
        key = cls._make_level_list_key(info['side'], info['price'])
        order_cache_data = '%s,%s,%s,%s,%s,%s' % (info['id'], info['price'], info['quantity'], info['leave_qty'], info['side'], info['t'])
        r = cls.rclient.lpush(key, order_cache_data)
        # add level key
        cls.set_order_book_level_key(info['side'], info['price'])
        order_list = cls.rclient.lrange(key, 0, -1)
        return [v.decode('utf-8').split(',') for v in order_list]

    @classmethod
    def update_order_to_side_order_book(cls, order):
        key = cls._make_level_list_key(order[4], order[1])
        cls.rclient.rpop(key)
        if float(order[3]) > 0:
            update_cache_data = ','.join(order)
            cls.rclient.rpush(key, update_cache_data)

        order_list = cls.rclient.lrange(key, 0, -1)
        if order_list:
            return [v.decode('utf-8').split(',') for v in order_list]
        # delete level key
        cls.delete_order_book_level_key(order[4], order[1])
        return []

    @classmethod
    def delete_order_to_side_order_book(cls, info):
        key = cls._make_level_list_key(info['side'], info['price'])
        lv_list = cls.get_level_order_list(info['side'], info['price'])
        update_lv_list = [x for x in lv_list if int(x[0]) != int(info['id'])]
        cls.rclient.delete(key)
        updated_list = [','.join(x) for x in update_lv_list]
        if updated_list:
            cls.rclient.rpush(key, *updated_list)
            if update_lv_list:
                return update_lv_list
        # delete level key
        cls.delete_order_book_level_key(info['side'], info['price'])
        return []

    @classmethod
    def clean_cache(cls):
        return [cls.rclient.delete(k) for k in cls.rclient.keys("%s:*" % cls.PREFIX)]



