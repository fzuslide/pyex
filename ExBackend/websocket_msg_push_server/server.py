#coding=utf-8

import os
import sys

abs_path = os.path.abspath(__file__)
project_path = '/'.join(abs_path.split('/')[:-2])
sys.path.insert(0, project_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ExBackend.settings")

import django
django.setup()

import time
import asyncio
import aioredis
import simplejson
import websockets
import logging
import logging.handlers

from functools import partial
from django.conf import settings


# Logger
ch = logging.handlers.TimedRotatingFileHandler(os.path.join(settings.REDIS_ROUTER_LOG_PATH, settings.REDIS_ROUTER_LOG_FILE), 'midnight', 1)
ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
ch.suffix = '%Y%m%d'
for name in ['']:
    logger = logging.getLogger(name)
    logger.setLevel(settings.REDIS_ROUTER_LOG_LEVEL)
    logger.addHandler(ch)


MESSAGER_HOST = str(settings.DEFAULT_REDIS_MESSAGE_HOST)
MESSAGER_PORT= settings.DEFAULT_REDIS_MESSAGE_PORT
MESSAGER_CHANNEL = str(settings.DEFAULT_REDIS_MESSAGE_CHANNEL)

class Singleton(object):
    def __init__(self, cls):
        self._cls = cls
        self._instance = {}

    async def __call__(self, *args,**kwargs):
        if self._cls not in self._instance:
            self._instance[self._cls] = self._cls(*args,**kwargs)
            await self._instance[self._cls].redis_subscriber()
        return self._instance[self._cls]

@Singleton
class AsyncRedisConsumer(object):
    clients = set()
    
    def __init__(self, loop, host, port, channel, **kwargs):
        self.loop = loop
        self.host = host
        self.port = port
        self.channel = 'ch:%s' % channel
        self.sub = None
        self.sub_channel = None

    def add_client(self, client):
        if client not in self.clients:
            self.clients.add(client)

    async def redis_subscriber(self):
        if not self.sub:
            self.sub = await aioredis.create_redis('redis://%s:%s' % (self.host, self.port))
        if not self.sub_channel:
            res = await self.sub.subscribe(self.channel)
            self.sub_channel= res[0]
        futures = []
        futures.append(asyncio.ensure_future(self._raw_push(self.sub_channel)))
        if futures:
            await asyncio.wait(futures)

    async def _raw_push(self, ch):
        while (await ch.wait_message()):
            raw_msg = await ch.get_json()
            logging.debug('push raw msg: %s' % raw_msg)
            for websocket in self.clients:
                # Send the message with the same channel the client used to
                # subscribe.
                try:
                    await websocket.send(simplejson.dumps(raw_msg))
                except Exception as e:
                    logging.error('websocket send error: %s' % e)


async def msg_pusher_handler(loop, websocket, path):
    """
    msg pusher handler
    """
    while True:
        try:
            consumer = await AsyncRedisConsumer(loop, MESSAGER_HOST, MESSAGER_PORT, MESSAGER_CHANNEL)
            consumer.add_client(websocket)
            received_data = await websocket.recv()
            logging.debug('received : %s ' % (received_data)) 
        except websockets.exceptions.ConnectionClosedOK as e:
            import traceback; traceback.print_exc()
            logging.error('msg pusher wait error: %s'%e)
            consumer.clients.remove(websocket)
            break

if __name__ == '__main__':
    host = '0.0.0.0'
    port = 9900
    loop = asyncio.get_event_loop()
    start_server = websockets.serve(partial(msg_pusher_handler, loop), host, port)
    print(f"websocket server start at: {host}:{port}")

    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
