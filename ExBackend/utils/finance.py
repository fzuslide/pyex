#coding=utf-8

def round_up(value, ext_len=2):
    return round(value * (10**ext_len)) / (10**ext_len)
