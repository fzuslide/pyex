# -*- coding: utf-8 -*-

import fcntl
import os
import time
import datetime
import simplejson

from .timeutils import stamp_to_datetime
from django.conf import settings

def get_msg_time(msg):
    """
        从消息中提取消息时间
    """
    msg_type = msg['doc_type']
    if msg_type == 'StartGame':
        msg_time = stamp_to_datetime(msg['notify_time'])
    else:
        msg_time = datetime.datetime.now()
    return msg_time


def make_daily_path(msg, site_ip=settings.DEFAULT_DISKLOG_SITE_IP):
    msg_type = msg['doc_type']
    time_str = get_msg_time(msg).strftime('%Y%m%d')
    return '%s/%s_%s_%s.txt'%(msg_type, msg_type.lower(), site_ip, time_str)


def make_hourly_path(msg, site_ip=settings.DEFAULT_DISKLOG_SITE_IP):
    msg_type = msg['doc_type']
    time_str = get_msg_time(msg).strftime('%Y%m%d%H')
    return '%s/%s_%s_%s.txt'%(msg_type, msg_type.lower(), site_ip, time_str)


def write_disk_log(msg, app_disk_log_split=settings.DEFAULT_APP_DISK_LOG_SPLIT, app_disk_log_dir=settings.DEFAULT_APP_DISK_LOG_DIR):
    if app_disk_log_split == 'daily':
        path = make_daily_path(msg)
    elif app_disk_log_split == 'hourly':
        path = make_hourly_path(msg)

    abs_path = os.path.join(app_disk_log_dir, path)
    
    abs_dir = os.path.split(abs_path)[0]
    # 确保路径可写
    if not os.path.isdir(abs_dir):
        os.mkdir(abs_dir)

    # 写入消息的json字符串
    for i in range(10):
        try:
            f = open(abs_path, 'a')
            fcntl.lockf(f, fcntl.LOCK_EX)
            f.write('%s\n'%simplejson.dumps(msg))
            fcntl.lockf(f, fcntl.LOCK_UN)
            f.close()
            break
        except:
            import traceback; traceback.print_exc()
            time.sleep(0.0001)


