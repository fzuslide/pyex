#coding=utf-8

# INNER ERROR CODE in range (10000, 19999)
ERROR_CODE_DATABASE_CONN_ERROR = 10000
ERROR_CODE_DATABASE_INSERT_ERROR = 10001
ERROR_CODE_DATABASE_QUERY_ERROR = 10002
ERROR_CODE_DATABASE_DELETE_ERROR = 10003
ERROR_CODE_DATABASE_ACCESS_ERROR = 10004
ERROR_CODE_DATABASE_NOT_EXIST_ERROR = 10005


# Client ERROR CODE in range (20000, 29999)
ERROR_CODE_UNAUTHORIZED = 20000
ERROR_CODE_INVALID_ARGS = 20001
ERROR_CODE_UNDEFINED = 20002
ERROR_CODE_USER_IN_BLACKLIST = 20003

# Order ERROR CODE in range (30000, 39999)
ERROR_CODE_ORDER_INVALID_ARGS = 30001
ERROR_CODE_ORDER_CANNOT_CANCEL_ORDER= 30002




_error_map = {ERROR_CODE_DATABASE_CONN_ERROR: "%s: database can not be connection." % ERROR_CODE_DATABASE_CONN_ERROR,
              ERROR_CODE_DATABASE_INSERT_ERROR: "%s: An error occurred while inserting data to the database." % ERROR_CODE_DATABASE_INSERT_ERROR,
              ERROR_CODE_DATABASE_QUERY_ERROR: "%s: An error occurred while querying data from the database." % ERROR_CODE_DATABASE_QUERY_ERROR ,
              ERROR_CODE_DATABASE_NOT_EXIST_ERROR: "%s: Query item not exist." % ERROR_CODE_DATABASE_NOT_EXIST_ERROR,
              ERROR_CODE_UNAUTHORIZED: "%s: The request is not authorized." % ERROR_CODE_UNAUTHORIZED,
              ERROR_CODE_INVALID_ARGS: "%s: The arguments is invalid." % ERROR_CODE_INVALID_ARGS,
              ERROR_CODE_USER_IN_BLACKLIST: "%s: User in blacklist." % ERROR_CODE_USER_IN_BLACKLIST,

              ERROR_CODE_ORDER_INVALID_ARGS: "%s: The arguments is invalid." % ERROR_CODE_ORDER_INVALID_ARGS,
              ERROR_CODE_ORDER_CANNOT_CANCEL_ORDER: "%s: This order can not be canceled." % ERROR_CODE_ORDER_CANNOT_CANCEL_ORDER,
              }



def get_error_desp(code):
    """
    获取错误描述
    """
    return _error_map.get(code, "unknown error code")


