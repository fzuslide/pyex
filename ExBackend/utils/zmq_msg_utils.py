#coding=utf-8

import os
import sys

import zmq
import simplejson
import logging
import logging.handlers

from django.conf import settings

# 设置日志
ch = logging.handlers.TimedRotatingFileHandler(os.path.join(settings.ZMQ_LOG_PATH, settings.ZMQ_SENDER_LOG_FILE), 'midnight', 1)
ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
ch.suffix = '%Y%m%d'
for name in ['']:
    logger = logging.getLogger(name)
    logger.setLevel(settings.ZMQ_LOG_LEVEL)
    logger.addHandler(ch)

context = zmq.Context()

zmq_msg_sender = context.socket(zmq.PUSH)
zmq_msg_sender.bind('tcp://%s:%s'%(settings.DEFAULT_ZMQ_HOST, settings.DEFAULT_ZMQ_PORT))


def zmq_dispatch_msg(msg):
    logging.debug('dispatch msg: %s'%(msg))
    zmq_msg_sender.send(simplejson.dumps(msg).encode("utf-8"))



