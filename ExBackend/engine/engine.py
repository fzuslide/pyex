#coding=utf-8

import os
import sys
import datetime

abs_path = os.path.abspath(__file__)
project_path = '/'.join(abs_path.split('/')[:-2])
sys.path.insert(0, project_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ExBackend.settings")

import django
django.setup()

from django.db.models import Q

from order.order_book import OrderBookCache
from order.models import UserOrder


class Engine(object):
    """
    Order match Engine

    order json:
    order = {
        'id': 6,
        'user': '1',
        'side': '1',
        'price': '8000.50',
        'quantity': '1.00',
        'leave_qty': '1.00',
        'status': 0,
        'order_type': 1,
        't': 1582887664
    }

    match_order_list = [[id, price, quantity, leave_qty, side, timestamp], ...]
    """

    def __init__(self, order):
        self.order = order
        self.is_filled = False
        self.publish_msg_list = []

    def _make_order_list_publish_msg(self, side, price, order_list):
        if order_list:
            action = 'update'
            leave_qty = OrderBookCache.cal_order_total_leave_qty(order_list)
            body = {'side': int(side), 'price': float(price), 'quantity': leave_qty}
        else:
            action = 'delete'
            body = {'side': int(side), 'price': float(price)}
        msg = {
                'msg_item': 'order_book',
                'action': action,
                'body': body
                }
        self.publish_msg_list.append(msg)

    def _fill_order(self, match_order):
        # filled 

        if float(match_order[3]) >= float(self.order['leave_qty']):
            # update model
            UserOrder.update_order_status(self.order['id'], 0)
            # update cache lv list
            match_leave_qty = float(match_order[3]) - float(self.order['leave_qty'])
            # filled match_order db
            UserOrder.update_order_status(match_order[0], match_leave_qty)

            match_order[3] = '%s' % match_leave_qty
            order_list = OrderBookCache.update_order_to_side_order_book(match_order)
            self._make_order_list_publish_msg(match_order[4], match_order[1], order_list)
            return True
    
        # partial_fill
        else:
            self.order['leave_qty'] = float(self.order['leave_qty']) - float(match_order[3])
            match_order[3] = '0'
            # filled match_order db
            UserOrder.update_order_status(match_order[0], 0)
            order_list = OrderBookCache.update_order_to_side_order_book(match_order)
            return False


    def process_order(self):
        # check 'can match' level
        match_side = int(self.order['side']) * -1
        level_key_list = OrderBookCache.get_can_math_level_key_list(match_side, float(self.order['price']))
        # fill order, and pop filled order
        for item in level_key_list:
            price = item[0].decode('utf-8')
            match_order_list = OrderBookCache.get_level_order_list(match_side, price)
            for match_order in match_order_list:
                if self._fill_order(match_order):
                    self.is_filled = True
                    return True 
            # append delete order book level msg
            self._make_order_list_publish_msg(match_order[4], match_order[1], [])

        # if not filled, append to order book cache
        order_list = OrderBookCache.add_order_to_side_order_book(self.order) 
        self._make_order_list_publish_msg(self.order['side'], self.order['price'], order_list)
        # update UserOrder model
        UserOrder.update_order_status(self.order['id'], self.order['leave_qty'])
        return False

    def process_cancel(self):
        # delete cache
        order_list = OrderBookCache.delete_order_to_side_order_book(self.order)
        self._make_order_list_publish_msg(self.order['side'], self.order['price'], order_list)
        return True



if __name__ == '__main__':
    #print('buy  order book: ', OrderBookCache.get_side_order_book(1, 5))
    #print('sell order book: ', OrderBookCache.get_side_order_book(-1, 5))
    #print('buy: ', OrderBookCache.get_can_math_level_key_list(-1, 9000))
    #print('sell: ', OrderBookCache.get_can_math_level_key_list(1, '9001.50'))
    #order = {'user': 1, 'side': 1, 'price': '8011.50', 'quantity': '3.5', 'leave_qty': '3.5', 'status': 0, 'order_type': 1, 'id': 6, 't': 1582887664}
    #order = {'user': '1', 'side': '-1', 'price': '8000.5', 'quantity': '5', 'leave_qty': '5', 'status': 0, 'order_type': '1', 'id': 259, 't': 1583048186}
    print('buy order book: ', OrderBookCache.get_side_order_book(1, 5))
    order = {'user': 1, 'side': 1, 'price': '8000.50', 'quantity': '1.00', 'leave_qty': '1.00', 'status': 3, 'order_type': 1, 'id': 349, 't': 1583052465}
    engine = Engine(order)
    engine.process_cancel()
    print('msg: ', engine.publish_msg_list)
    
