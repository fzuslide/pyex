#!/bin/bash

set -e

##
# DEFINE
##

APP=ExBackend
PYTHON_EXEC=python3

PROJECT_PATH=$(cd $(dirname $(realpath "$0")); cd ..;  pwd -P)
echo $PROJECT_PATH
VENV_NAME=venv_${APP}

start(){
    init_venv
}


init_venv(){
    mkdir -p ~/logs
    cd "${PROJECT_PATH}"
    ${PYTHON_EXEC} -m venv ${VENV_NAME}
    source ${VENV_NAME}/bin/activate
    echo "pip install pre requires"
    pip install -U wheel
    echo "pip installing requirements...."
    pip install -U -r requirements.txt
    echo "pip complete"
}

case "${1:-''}" in
    'start')
        start
        ;;

    '_run')
        eval "$2"
        ;;
    *)
        echo "usage: $0 start"
        ;;
esac
