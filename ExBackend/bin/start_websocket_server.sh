#!/bin/bash

set -e

##
# DEFINE
##

APP=ExBackend
PYTHON_EXEC=python3

PROJECT_PATH=$(cd $(dirname $(realpath "$0")); cd ..;  pwd -P)
echo $PROJECT_PATH
VENV_NAME=venv_${APP}

start(){
    cd "${PROJECT_PATH}"
    source ${VENV_NAME}/bin/activate
    python websocket_msg_push_server/server.py
}


start
