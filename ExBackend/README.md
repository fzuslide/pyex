## Change Configs 

Before start backend service, Please confirm that the configuration files are correct:
### `vim ExBackend/site_settings.py`


## Create VEnv 

In the project directory, you can run:

### `./bin/init_env.sh`


REST API:

### `./bin/start_rest_api.sh`


Exchange engine worker:

### `./bin/start_exchange_engine.s`


Websocket Server: 

### `./bin/start_websocket_server.sh`

