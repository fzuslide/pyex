#coding=utf-8

import xadmin
from django.contrib import admin
from django.utils.safestring import mark_safe
from import_export.admin import ExportMixin

from .models import UserProfile

class UserProfileAdmin(object):
    list_display = ("user", "name", "status", "open_date", "email", "mobile", "address", "nationality", "create_time", "update_time")
    search_fields = ("user", "name", "mobile")    

xadmin.site.register(UserProfile, UserProfileAdmin)
