# coding:utf-8

from django.apps import AppConfig
import os

default_app_config = 'user_profile.ClientConfig'

VERBOSE_APP_NAME = "用户资料管理"


class ClientConfig(AppConfig):
    name = "user_profile"
    verbose_name = VERBOSE_APP_NAME
