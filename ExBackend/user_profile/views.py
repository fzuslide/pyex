#coding=utf-8
"""
user Profile
"""

import logging
import datetime
import operator
from functools import reduce
from django.db import models
from django.conf import settings

import utils.errors as err
from utils.view_tools import ok_json, fail_json
from utils.abstract_api import AbstractAPI

from .models import UserProfile

logger = logging.getLogger(__name__)


class QueryProfileByClientIdAPI(AbstractAPI):
    def config_args(self):
        self.args = {
                'user_id': 'r',
                }

    def access_db(self, kwarg):
        try:
            obj = UserProfile.objects.filter(is_active=True, user_id=kwarg['user_id']).first()
            return obj, kwarg
        except Exception as e:
            logger.error('[class:%s] %s' % (self.__class__.__name__, e))
            return None, kwarg

    def format_data(self, data):
        obj, kwarg = data
        if obj:
            return ok_json(data=obj.get_json())
        return fail_json(err.ERROR_CODE_DATABASE_QUERY_ERROR)

query_profile_by_user_id_api = QueryProfileByClientIdAPI().wrap_func()


