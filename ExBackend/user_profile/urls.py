#coding=utf-8


from django.conf.urls import url
from .views import query_profile_by_user_id_api



urlpatterns = [
        url(r'^query/by/id/$', query_profile_by_user_id_api, name="query_profile_by_user_id_api"),
        ]

