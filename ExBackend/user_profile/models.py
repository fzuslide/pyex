#coding=utf-8

import json

from django.db import models
from django.core import serializers
from django.conf import settings
from django.db.models import Q
from django.contrib.auth.models import User


from utils.basemodel.base import BaseModel


class UserProfile(BaseModel):
    """
    User Extend Profile && KYC info
    """
    GENDER = [ 
            ('F', '女'),
            ('M', '男'),
            ('S', '未知'),
            ]   


    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    name = models.CharField('姓名', max_length=512)
    gender = models.CharField('性别',max_length=1, choices=GENDER, default=GENDER[2][0])
    id_no = models.CharField('身份证号码', max_length=32)
    birthday = models.DateField('生日', null=True, blank=True)
    mobile = models.CharField('联系电话', max_length=50, null=True, blank=True, db_index=True)
    email = models.CharField('Email', max_length=256, null=True, blank=True)
    address = models.CharField('地址', max_length=512, null=True, blank=True)
    nationality = models.CharField('国籍', max_length=64, null=True, blank=True)

    # operation info
    open_date = models.DateField('开户时间', null=True, blank=True)
    status = models.CharField('账户状态', max_length=32)
	
    class Meta:
        db_table = 'user_profile'
        verbose_name = '客户资料'
        verbose_name_plural = '客户资料'

    def __str__(self):
        return '%s:%s' % (self.user.id, self.name)
		
    def get_json(self, clean=True):
        serials = serializers.serialize("json", [self])
        struct = json.loads(serials)
        data = struct[0]['fields']
        if 'pk' in struct[0]:
            data['user_id'] = struct[0]['pk']
        if clean:
            data.pop('create_time')
            data.pop('update_time')
            data.pop('is_active')
        return data 

